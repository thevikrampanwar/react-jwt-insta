import React, { useEffect } from "react";
// import InstagramLogin from 'react-instagram-login';
import Cookies from "universal-cookie";

import { Card, Avatar } from "antd";

import InstagramLogin from "./InstagramLogin";
import axios from "axios";
import properties, { cookies } from "../config/properties";
import FormData from "form-data";
import _ from "lodash";
import { useRecoilState } from "recoil";
import { linkedInstaAccounts } from "../atom/globalState";
import { getInstaUserInfo, registerInstaUserInfo } from "../util/ApiUtil";

const { Meta } = Card;
const InstaLogin = (props) => {
  console.log("props in instalogin", props);
  const [linkedAccounts, setLinkedInstaAccounts] =
    useRecoilState(linkedInstaAccounts);

  useEffect(() => {
    //check for expiration of token as well
    if (localStorage.getItem("accessToken") === null) {
      props.history.push("/login");
    }
  }, []);

  const getInstaUserInfoFromService = (response) => {
    //store respose.data
    if (response.data.user_id) {
      console.log("setting up insta user staate ", response.data);
      localStorage.setItem("insta_user", response.data);
      cookies.set("insta_user", response.data, { path: "/" });
      console.log("cookies ", cookies.get("insta_user"));
      const fb_access_token = localStorage.getItem("accessToken");
      console.log("fb_access_token", fb_access_token);
      const user_info = registerInstaUserInfo(response.data)
        .then((res) => {
          console.log(res);
          setLinkedInstaAccounts([...linkedAccounts, res]);
        })
        .catch((err) => console.error("ERRR", err));

      console.log("user_info", user_info);
      props.history.push("/home");
      //redirecting to account.js
      // props.history.push("/me");
    }
  };

  const failedresponseInstagram = (responsek) => {
    console.log("instalogin failrue response");
    console.log(responsek);
    localStorage.setItem("instalogin", responsek);
  };

  const successResponse = (code) => {
    console.log("inside success func");
    console.log(code);
    var bodyFormData = new FormData();
    bodyFormData.append("redirect_uri", properties.INSTA_REDIRECT_URL);
    bodyFormData.append("code", code);
    bodyFormData.append("client_id", properties.INSTA_CLIENT_ID);
    bodyFormData.append("client_secret", properties.INSTA_CLIENT_SECRECT);
    bodyFormData.append("grant_type", "authorization_code");
    axios({
      method: "post",
      url: properties.INSTA_ACCESS_TOKEN_URL,
      data: bodyFormData,
      headers: {
        "Content-Type": "multipart/form-data",
        Accept: "application/vnd.api+json",
      },
    })
      .then(getInstaUserInfoFromService)
      .catch(function (response) {
        //handle error
        console.log("errr", response);
      });
  };
  return (
    <article style={{ display: "inline" }}>
      <InstagramLogin
        clientId={properties.INSTA_CLIENT_ID}
        buttonText="Login"
        redirectUri={properties.INSTA_REDIRECT_URL}
        scope="user_profile,user_media"
        onSuccess={successResponse}
        onFailure={failedresponseInstagram}
      />
      )
    </article>
  );
};

export default InstaLogin;

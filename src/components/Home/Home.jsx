import generateCalendar from "antd/lib/calendar/generateCalendar";
import React from "react";
import { useRecoilState } from "recoil";
import { linkedInstaAccounts } from "../../atom/globalState";
import { Card, Avatar, Row, Col, Divider, Typography } from "antd";
import {
  EditOutlined,
  EllipsisOutlined,
  SettingOutlined,
  ProfileOutlined,
} from "@ant-design/icons";

import "./Home.css";

const { Meta } = Card;

const { Title } = Typography;

const Home = () => {
  const [linkedAccounts, setLinkedInstaAccounts] =
    useRecoilState(linkedInstaAccounts);

  const getCard = (ac) => {
    return (
      <Col className="gutter-row">
        <Card
          style={{
            width: 420,
          }}
          actions={[
            <b>{ac.followers} followers</b>,
            <b>{ac.followings} following</b>,
          ]}
        >
          <Meta
            avatar={
              <Avatar
                src={ac.instaProfilePicUrl}
                className="user-avatar-circle"
              />
            }
            title={ac.fullName}
            description={"@" + ac.instaUserName}
          />
        </Card>
      </Col>
    );
  };

  return (
    <>
      <Divider dashed orientation="center">
        <Title level={2}>Connected Accounts</Title>
      </Divider>
      <Row justify="center" gutter={[16, 24]}>
        {linkedAccounts.map((ac) => getCard(ac))}
      </Row>
    </>
  );
};

export default Home;

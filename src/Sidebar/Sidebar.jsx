import React, { useState, useEffect } from "react";
import "./Sidebar.css";
import { Link } from "react-router-dom";
import { useRecoilState } from "recoil";
import { Avatar } from "antd";

import {
  HeartFilled,
  HomeFilled,
  BarsOutlined,
  MessageOutlined,
  UserOutlined,
  LogoutOutlined,
  PlusCircleOutlined,
} from "@ant-design/icons";

import { getCurrentUser } from "../util/ApiUtil";
import { linkedInstaAccounts, loggedInUser } from "../atom/globalState";
import { program } from "@babel/types";
import _ from "lodash";
import recoilPersist from "recoil-persist";

const Sidebar = (props) => {
  const [isInactive, setInactive] = useState(true);
  const [selectedRoute, setSelectedRoute] = useState("");
  const [profile, setProfile] = useRecoilState(loggedInUser);

  console.log(profile);

  const [currentUser, setLoggedInUser] = useRecoilState(loggedInUser);
  const [linkedAccounts, setLinkedAccounts] =
    useRecoilState(linkedInstaAccounts);

  useEffect(() => {
    if (!localStorage.getItem("accessToken")) {
      props.history.push("/login");
    }
    loadCurrentUser();
  }, []);

  const loadCurrentUser = () => {
    getCurrentUser()
      .then((response) => {
        setLoggedInUser(response);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const logout = () => {
    setProfile({});
    setLoggedInUser({});
    setLinkedAccounts([]);

    localStorage.clear();
    // props.history.push("/");
    // window.location = "/";
  };

  const sidebarClass = isInactive ? "nav" : "nav inactive";

  if (!localStorage.getItem("accessToken") || _.isEmpty(profile)) {
    return null;
  }

  return (
    <nav className={sidebarClass}>
      <button
        className="nav__button clickable"
        onClick={() => setInactive(!isInactive)}
      >
        <BarsOutlined />
      </button>
      <Avatar src={profile.profilePicture} className="nav__avatar avatar" />

      <p className="nav__name">{profile.name}</p>

      <ul className="nav__list">
        <Link className="link" to="/notifications">
          <li
            className={selectedRoute == "notifications" ? "selected" : ""}
            onClick={() => setSelectedRoute("notifications")}
          >
            <HeartFilled className="list__icons clickable" />
            <span className="text">Notifications</span>
          </li>
        </Link>
        <Link className="link" to="/home">
          <li
            className={selectedRoute == "home" ? "selected" : ""}
            onClick={() => setSelectedRoute("home")}
          >
            <HomeFilled className="list__icons clickable" />
            <span className="text">Home</span>
          </li>
        </Link>

        <Link className="link" to="/chat">
          <li
            className={selectedRoute == "chat" ? "selected" : ""}
            onClick={() => setSelectedRoute("chat")}
          >
            <MessageOutlined className="list__icons clickable" />
            <span className="text">Chat</span>
          </li>
        </Link>

        <Link className="link" to="/instalogin">
          <li
            className={selectedRoute == "add" ? "selected" : ""}
            onClick={() => setSelectedRoute("add")}
          >
            <PlusCircleOutlined className="list__icons clickable" />
            <span className="text">Add</span>
          </li>
        </Link>

        <Link className="link" to="/login">
          <li className="" onClick={() => logout()}>
            <LogoutOutlined className="list__icons clickable" />
            <span className="text">Log Out</span>
          </li>
        </Link>
      </ul>
    </nav>
  );
};

export default Sidebar;
